'''
Created on Jun 18, 2014

@author: francis
'''

from Depends import dep, depends, isSatisfied, ndep, satisfy, strict, lazy, \
    verify, flatten, satisfied, unsatisfied, allSatisfied, allReady, ready, Ref
from Errors import CircularDependencyError, InvalidDependantName, DependsError
import sys

__author__ = 'Francis Horsman'
__version__ = '1.8'
__email__ = 'francis.horsman@gmail.com'
__doc__ = 'A dependency graph creator and checker'

del sys
