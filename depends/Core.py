# -*- coding: utf-8 -*-
'''
Created on Jul 6, 2014

@author: francis
'''

import itertools

from .Errors import CircularDependencyError, InvalidDependantName

class depends(object):
    _uId = itertools.count(0)
    '''
    @summary: The core depends class.
    '''
    def __init__(self, ref, strict=False):
        '''
        @param ref: The reference for this depends. In strict mode, this
        must start with a character, not a number or underscore.
        @param strict: True = Enable strict mode, False = otherwise.
        @raise InvalidDependantName: 'ref' is invalid if Strict is True.
        '''
        if strict:
            _checkIsValidRef(ref)
        self.__dict__ = {'__ref': ref,
                         '__strict':strict,
                         '__satisfied':False,
                         '__parent':[],
                         '__uId': depends._uId.next()
                         }
        self._dependencies = {}
    def __delitem__(self, name):
        return self.__del(name)
    def __getitem__(self, name):
        if not isinstance(name, depends):
            name = depends(name)
        return self._dependencies[_Ref(name)]
    def __setitem__(self, name, value):
        if not isinstance(value, depends):
            value = depends(value)
        self._dependencies[name] = value
        _parent(value, self)
    def __setattr__(self, name, value):
        if name in ['__dict__', '_dependencies']:
            return object.__setattr__(self, name, value)
        else:
            if not isinstance(value, depends):
                value = depends(value)
            key = _Ref(getattr(self, name))
            self._dependencies[key] = value
            _parent(value, self)
    def __getattr__(self, name):
        i = [(k, v) for k, v in self._dependencies.items()]
        for k, v in i:
            if name == k:
                return v
        return object.__getattribute__(self.__dict__, name)
    def __delattr__(self, name):
        return self.__del(name)
    def __del(self, name):
        if not isinstance(name, depends):
            name = depends(name)
        try:
            del self._dependencies[_Ref(name)]
        except:
            if _isStrict(self):   raise
    def __add__(self, other):
        if not isinstance(other, list):
            other = [other]
        for (index, item) in enumerate(other):
            if not isinstance(item, depends):
                other[index] = depends(item)
            ref_ = _Ref(other[index])
            if _isStrict(self) and ((ref_ in self._dependencies.keys()) or (_searchParentDependencies(ref_, self))):
                raise CircularDependencyError(ref_)
            self._dependencies[_Ref(other[index])] = other[index]
            _parent(other[index], self)
            _autoVerify(self)
            _autoVerify(other[index])
        return self
    def __sub__(self, other):
        if not isinstance(other, list):
            other = [other]
        for (index, item) in enumerate(other):
            if not isinstance(item, depends):
                other[index] = depends(item)
            if _Ref(other[index]) not in self._dependencies.keys():
                if _isStrict(self):
                    raise KeyError(_Ref(other[index]))
            else:
                _nparent(self._dependencies[_Ref(other[index])], self)
                del self._dependencies[_Ref(other[index])]
        return self
    def __contains__(self, item):
        if isinstance(item, depends):
            item = _Ref(item)
        x = [i for i in self._dependencies.keys()]
        return item in x
    def __eq__(self, item):
        if _verify(self) == _verify(item):
            if _Ref(self) == _Ref(item):
                return True
        return False
    def __ne__(self, item):
        return not self == item
    def __iter__(self):
        return iter(self._dependencies.keys())
    def __str__(self):
        return _toString(self, noraise=True)

class _Found(Exception):
    pass

def _searchParentDependencies(ref_, obj):
    try:
        __searchParentDependencies(ref_, obj)
    except (_Found, CircularDependencyError) as _e:
        return True
    return False

def __searchParentDependencies(ref_, obj, parents=None):
    if parents is None:
        parents = []
    parents_ = _getParents(obj)
    for parent in parents_:
        parentRef = _Ref(parent)
        if ref_ == parentRef:
            raise _Found(ref_)
        parentRefs = [_Ref(i) for i in parents[:-1]]
        if ref_ in parentRefs:
            raise _Found(ref_)
        if parentRef in parentRefs:
            raise _Found(parentRef)
        else:
            __searchParentDependencies(ref_, parent, parents=(parents + [parent]))
    pass

def _Ref(deps):
    return deps.__dict__['__ref']

def _isSatisfied(deps):
    return deps.__dict__['__satisfied'] is True

def _setSatisfied(deps, enabler=True):
    deps.__dict__['__satisfied'] = True

def _isStrict(deps):
    return deps.__dict__['__strict']

def _setIsStrict(deps, enabler):
    deps.__dict__['__strict'] = enabler

def _setStrict(deps, enabler, verify_, noraise, recursive):
    depsO = deps
    if deps is None:
        raise ValueError('No depends specified')
    if not isinstance(deps, list):
        deps = [deps]
    for i in deps:
        _setIsStrict(i, enabler)
        if verify_ is True:
            _verify(i, noraise=noraise)
    return depsO

def _verify(deps, noraise=False, createRef=None):
    id_ = id(deps)
    r = _Ref(deps)
    d = _walk(deps, [id_, r], noraise=noraise, createRef=createRef)
    if isinstance(d, basestring):
        return d
    return d[r]

def _autoVerify(deps):
    if _isStrict(deps):
        _verify(deps)

def _satisfy(deps, item, depsVisited=None, noraise=False):
    if not isinstance(item, depends):
        item = depends(item)
    if depsVisited is None:
        depsVisited = []
    ref_ = _Ref(deps)
    if ref_ == _Ref(item):
        _setSatisfied(deps)
    for v in deps._dependencies.values():
        id_ = id(v)
        if isinstance(v, depends):
            ref_ = _Ref(v)
            if (id_ in depsVisited) or (ref_ in depsVisited):
                try:
                    raise CircularDependencyError(ref_)
                except CircularDependencyError:
                    if noraise is False:
                        raise
            else:
                if ref_ == _Ref(item):
                    _setSatisfied(v)
                _satisfy(v, item, depsVisited+[ref_, id_], noraise)

def _ready(deps, depsVisited=None, noraise=False, ignoreRoot=False):
    if not isinstance(deps, depends):
        deps = depends(deps)
    if depsVisited is None:
        depsVisited = []
    ref = _Ref(deps)
    #    Are the immediate dependencies satisfied?
    values = deps._dependencies.values()
    if values:
        if all([_isSatisfied(i) for i in values]) is True:
            if not _isSatisfied(deps):
                yield (ref, deps)
        #    Check our children:
        for v in values:
            id_ = id(v)
            if isinstance(v, depends):
                ref = _Ref(v)
                try:
                    sd = (_searchParentDependencies(ref, v))
                except (Exception, RuntimeError) as _e:
                    raise
                if (ref in v._dependencies.keys()) or sd or (id_ in depsVisited) or (ref in depsVisited):
                    try:
                        raise CircularDependencyError(ref)
                    except CircularDependencyError:
                        if noraise is False:
                            raise
                else:
                    for i in _ready(v, depsVisited=depsVisited+[ref, id_], noraise=noraise, ignoreRoot=False):
                        yield i
    else:
        if not _isSatisfied(deps):
            yield (ref, deps)

def _yieldSatisfied(deps, depsVisited=None, noraise=False, queryStatus=False, ignoreRoot=False):
    '''
    @summary: yield a list of satisfied dependencies
    '''
    if not isinstance(deps, depends):
        deps = depends(deps)
    if depsVisited is None:
        depsVisited = []
    ref = _Ref(deps)
    if _isSatisfied(deps) == queryStatus:
        if ignoreRoot is False:
            yield (ref, deps)
    for v in deps._dependencies.values():
        id_ = id(v)
        if isinstance(v, depends):
            ref = _Ref(v)
            try:
                sd = (_searchParentDependencies(ref, v))
            except (Exception, RuntimeError) as _e:
                raise
            if (ref in v._dependencies.keys()) or sd or (id_ in depsVisited) or (ref in depsVisited):
                try:
                    raise CircularDependencyError(ref)
                except CircularDependencyError:
                    if noraise is False:
                        raise
            else:
                for i in _yieldSatisfied(v, depsVisited=depsVisited+[ref, id_], noraise=noraise, queryStatus=queryStatus, ignoreRoot=False):
                    yield i

def _walk(deps, depsVisited=None, noraise=False, createRef=None):
    def _createRef(ref):
        return {ref: []}
    if createRef is None:
        createRef = _createRef
    if depsVisited is None:
        depsVisited = []
    ref = _Ref(deps)
    try:
        dependencies = []
        result = {ref: dependencies}
        for _k, v in sorted(deps._dependencies.items()):
            id_ = id(v)
            ref_ = _Ref(v)
            sd  = _searchParentDependencies(ref_, v)
            idv = (id_ in depsVisited)
            dv = (ref_ in depsVisited)
            rv = (ref_ in v._dependencies.keys())
            if rv or sd or idv or dv:
                try:
                    raise CircularDependencyError(ref_)
                except CircularDependencyError as e:
                    if noraise is True:
                        dependencies.append(str(e))
                    else:
                        raise
            else:
                dependencies.append(_walk(v, depsVisited+[id_, ref_], noraise, createRef))
        if len(dependencies) == 0:
            result = createRef(ref)
        return result
    except CircularDependencyError as e:
        e.chain.insert(0, ref)
        raise

def _getParents(deps):
    return deps.__dict__['__parent']

def _parent(deps, parent):
    deps.__dict__['__parent'].append(parent)

def _nparent(deps, parent):
    deps.__dict__['__parent'].remove(parent)

def _checkIsValidRef(ref):
    if not isinstance(ref, basestring) or ref.startswith('_'):
        raise InvalidDependantName(ref)

def _toString(deps, depsVisited=None, noraise=False):
    strict = 'STRICT' if _isStrict(deps) else 'LAZY'
    satisfied = 'SATISFIED' if _isSatisfied(deps) else ''
    if strict and satisfied:
        ss = '|'.join([strict, satisfied])
    else:
        ss = strict or satisfied
    s = '(' if ss else ''
    end = ')' if ss else ''

    if depsVisited is None:
        depsVisited = []
    ref = deps.__dict__['__ref']
    try:
        dependencies = []
        sd = ''
        if len(deps._dependencies.items()) == 0:
            sd = '-'
        else:
            for _k, v in sorted(deps._dependencies.items()):
                id_ = id(v)
                ref = _Ref(v)
                try:
                    sd = (_searchParentDependencies(ref, v))
                except (Exception, RuntimeError) as _e:
                    raise
                if (ref in v._dependencies.keys()) or sd or (id_ in depsVisited) or (ref in depsVisited):
                    try:
                        raise CircularDependencyError(ref)
                    except CircularDependencyError as e:
                        if noraise is True:
                            dependencies.append(str(e))
                        else:
                            raise
                else:
                    dependencies.append(_toString(v, depsVisited+[ref, id_], noraise))
            if dependencies:
                sss = []
                for v in dependencies:
                    sss.append(v)
                    sss.append(', ')
                sd = ''.join(sss)
                sd = sd.replace(', ]', ' ]')
        z = ''.join([ss, s, str(_Ref(deps)), ': [', sd, ']', end])
        z = z.replace('[-]', '-')
        z = z.replace('), ])', ')])')
        return z
    except CircularDependencyError as e:
        e.chain.insert(0, ref)
        raise

def _iterateArgs(args):
    args = list(args)
    for index, i in enumerate(args):
        if not isinstance(i, list):
            args[index] = [i]
    return list(itertools.chain(*args))

def _flatten(deps, noraise=False):
    try:
        return _verify(deps, noraise=False, createRef=lambda x: x)
    except CircularDependencyError as e:
        if noraise is False:
            raise
        return e

def _graphPath(deps, path):
    yield [path[0]]
    if not isinstance(deps, basestring):
        for i in __graphPath(deps, path):
            yield i

def __graphPath(deps, path):
    def createPath(a, b=None):
        r = [i for i in a]
        if b is not None:
            r.append(b)
        return r
    for i in sorted(deps):
        if not isinstance(i, dict):
            yield createPath(path, i)
        elif isinstance(i, dict):
            for (k, v) in sorted(i.items()):
                path.append(k)
                yield createPath(path)
                for m in __graphPath(v, path):
                    yield m
                path.remove(k)

if __name__ == '__main__':
    pass
