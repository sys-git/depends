# -*- coding: utf-8 -*-
'''
Created on Jun 9, 2014

@author: francis
'''

from .Core import depends, _graphPath, _flatten, _isSatisfied, _iterateArgs, \
    _ready, _Ref, _satisfy, _setSatisfied, _setStrict, _verify, _yieldSatisfied
from .Errors import DependsError

def dep(a, *args):
    '''
    @summary: Make a depend on b.
    @param a: The item to assign the dependency to.
    @param args: The dependencies - lists and or individuals refs/depends.
    @return: a - allows for nested building: dep(a, dep(b, dep(c, d))).
    @raise exception: KeyError if any of args not in a and a is STRICT.
    '''
    if not isinstance(a, depends):
        a = depends(a)
    for i in _iterateArgs(args):
        a += i
    return a

def ndep(a, *args):
    '''
    @summary: Make a not depend on b.
    @param a: The item to unassign the dependencies from.
    @param args: The dependencies - lists and or individuals refs/depends.
    @return: a.
    @raise exception: KeyError if b not in a and a is STRICT.
    '''
    if not isinstance(a, depends):
        a = depends(a)
    for i in _iterateArgs(args):
        a -= i
    return a

def strict(deps, enabler=True, verify=False, noraise=False, recursive=False):
    '''
    @summary: Make deps STRICT or LAZY
    @param deps: The item to change.
    @param enabler: True = STRICT, FALSE = LAZY.
    @param verify: True = perform explicit verification post set. FALSE = No explicit verification.
    @param noraise: True = Don't raise errors, FALSE = Raise errors if they occur.
    @return: deps.
    @raise exception: CircularDependencyError if applicable.
    '''
    return _setStrict(deps, enabler, verify, noraise, recursive)

def lazy(deps, verify=False, noraise=False, recursive=False):
    '''
    @summary: Make deps LAZY
    @param deps: The item to change.
    @param verify: True = perform explicit verification post set. FALSE = No explicit verification.
    @param noraise: True = Don't raise errors, FALSE = Raise errors if they occur.
    @return: deps.
    @raise exception: CircularDependencyError if applicable.
    '''
    return _setStrict(deps, False, verify, noraise, recursive)

def verify(deps, noraise=False, createRef=None):
    '''
    @summary: Verify deps.
    @param deps: The item to change.
    @param enabler: True = STRICT, FALSE = LAZY.
    @param verify: True = perform explicit verification post set. FALSE = No explicit verification.
    @param noraise: True = Don't raise errors, FALSE = Raise errors if they occur.
    @return: A dictionary containing: {deps.ref:dependencies}.
    @raise exception: CircularDependencyError if applicable.
    '''
    return _verify(deps, noraise=noraise, createRef=createRef)

def flatten(deps, noraise=False):
    '''
    @attention: A generator
    @summary: Flatten the graph yielding lists of ordered dependencies:
                a
                a.b
                a.b.c
                a.b.d
                a.e
                etc...
    '''
    try:
        return _graphPath(_flatten(deps), path=[Ref(deps)])
    except DependsError as e:
        if noraise is False:
            raise
        return

def satisfy(deps, *args):
    '''
    @summary: Satisfy the given graph items - all occurances in the entire graph:
    @param deps: The item to satisfy.
    @param args: The dependencies - lists and or individuals refs/depends.
    a.b.c
    a.b.d
    a.c
    '''
    if deps is None:
        raise ValueError('No depends specified')
    if args:
        for item in _iterateArgs(args):
            _satisfy(deps, item)
    else:
        _setSatisfied(deps, enabler=True)

def isSatisfied(*args):
    '''
    @summary: Are the args depends satisfied?
    @param args: The dependencies - lists and or individuals refs/depends.
    '''
    if args is None:
        raise ValueError('No depends specified')
    return [_isSatisfied(i) for i in _iterateArgs(args)]

def Ref(deps):
    '''
    @summary: Determine the 'ref' of a depends (what it was constructed with).
    '''
    return _Ref(deps)

def satisfied(deps, noraise=False, ignoreRoot=False):
    '''
    @summary: yield a list of satisfied dependencies
    @param deps: The depends to query.
    @param noraise - True: Don't raise explicit exceptions, False: otherwise.
    @param ignoreRoot: True - ignore deps' its self, False - otherwise.
    '''
    for i in _yieldSatisfied(deps, noraise=noraise, queryStatus=True, ignoreRoot=ignoreRoot):
        yield i

def unsatisfied(deps, ignoreRoot=False):
    '''
    @summary: yield a list of unsatisfied dependencies
    @param deps: The depends to query.
    @param ignoreRoot: True - ignore deps' its self, False - otherwise.
    '''
    for i in _yieldSatisfied(deps, queryStatus=False, ignoreRoot=ignoreRoot):
        yield i

def allSatisfied(deps, ignoreRoot=False):
    '''
    @summary: Are all dependencies satisfied?
    @param deps: The depends to query.
    @param ignoreRoot: True - ignore deps' its self, False - otherwise.
    @return: True: All satisfied for the entire sub-tree, False: otherwise.
    '''
    for _ in _yieldSatisfied(deps, queryStatus=False, ignoreRoot=ignoreRoot):
        return False
    return True

def allReady(deps, *args, **kwargs):
    '''
    @summary: Are all the dependencies for a given depends satisfied in the entire sub-tree, 
    NOT including the root 'deps' depends.
    @param args: The dependencies - lists and or individuals refs/depends.
    @param kwargs: 'noraise' - True: Don't raise explicit exceptions, False: otherwise.
    @return: True: All ready for the entire sub-tree, False: otherwise.
    '''
    if not isinstance(deps, depends):
        if kwargs.get('noraise', False) is False:
            raise ValueError('No depends specified')
        return True
    #    Done this way for speed - because 'unsatisfied' is a generator:
    for _ in unsatisfied(deps, ignoreRoot=True):
        return False
    return True

def ready(deps, noraise=False):
    '''
    @summary: Find all the dependencies for a given depends which themselves
    have satisfied dependencies but which themselves are not satisfied.
    ie: Are ready to be satisfied.
    @param noraise - True: Don't raise explicit exceptions, False: otherwise
    @return: A list of tuples: [(ref, depends)].
    '''
    if not isinstance(deps, depends):
        if noraise is True:
            raise ValueError('No depends specified')
        return []
    return list(_ready(deps, noraise=noraise))

if __name__ == '__main__':
    pass
